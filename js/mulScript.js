var exData = {};
var userid = 0;
var companyid = 0;
var tag = null;
var activeResumeMode = "";
var settings = {};
var urllist = [];
var namelist = [];
var totalProfiles = 0;
var skippedProfile = 0;
var profileSuccess = 0;
var profileExist = 0;



var totalProfile = 0;
chrome.storage.sync.set({extractedObj: {}}, function () {
});

chrome.storage.sync.get('userid', function (usrid) {
    userid = usrid['userid'];
});

chrome.storage.sync.get('companyid', function (cmpid) {
    companyid = cmpid['companyid'];
});
chrome.storage.sync.get('tag', function (t) {
    tag = t['tag'];
});
chrome.storage.sync.get('activeResumeMode', function (arm) {
    activeResumeMode = arm['activeResumeMode'];
});

chrome.storage.sync.get('settingsObj', function (arm) {
    settings = arm['settingsObj'];
});


var status = 0;
/* Check if profile exists, skip the profile */
var checkProfileExist = function (profileurl, mode) {
    var url = "https://crmapp.alore.io:8080/socialdatasave/checkexist";
    var toSendObj = {};
    toSendObj.userid = userid;
    toSendObj.companyid = companyid;
    toSendObj.profileurl = profileurl;
    toSendObj.mode = mode;
    var http = new XMLHttpRequest();
    http.open("POST", url, true);
    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/json");
    http.onreadystatechange = function () {//Call a function when the state changes.
        if (http.readyState === 4 && http.status === 200) {
            /* When profile is not found */
            status = http.status;
        } else if (http.readyState === 4 && http.status == 401) {
            /* When profile is found */
            status = http.status;
        }
    };
    http.send(JSON.stringify(toSendObj));
};

/* clear storage with companywebsiteUrl key */
function clear() {
    for (var r = 0; r < 10; r++) {
        localStorage.removeItem('companyWebsiteUrl');
        chrome.storage.sync.clear(function (companyWebsiteUrl) {
        });
        chrome.storage.local.remove('companyWebsiteUrl');
        chrome.storage.sync.clear();
    }
}

/* This function gets company linkendIn url related to the user */
function getCompanyLinkedInUrl() {
    var companyProfileId;
    try {
        companyProfileId = document.querySelectorAll('[data-control-name="background_details_company"]')[0].getAttribute("href");
    } catch (err) {
        try {
            companyProfileId = document.querySelectorAll('[data-control-name="background_details_company"]')[0].getAttribute("href");
            if (companyProfileId === '') {
                companyProfileId = document.querySelectorAll('[data-control-name="background_details_company"]')[0].pathname;
            }
        } catch (err1) {
            //alert("please wait for loading page or make sure profile cotains company information.");
        }
    }
    return "https://www.linkedin.com" + companyProfileId;
}

var getAllLinksInPage = function () {
    window.scrollTo(0, 1200);
    totalProfiles = document.getElementsByClassName("name actor-name");
    for (var j = 0; j < totalProfiles.length; j++) {
        var profileNm = document.getElementsByClassName("actor-name")[j].innerHTML;
        var profileUrl = document.getElementsByClassName("search-result__image-wrapper")[j].children[0].href;
        namelist.push(profileNm);
        urllist.push(profileUrl);
        totalProfile++;
        chrome.storage.sync.set({'totalProfile': totalProfile}, function () {
        });
        localStorage.setItem("totalProfile", JSON.stringify({totalProfile: totalProfile}));
    }
};

/* to get LinkedIn Search people data */
var linkedinCompanyUrl = "";
var profileName = "";
var companyName = "";
var colgName = "";
var userLocation = "";
var designation = "";
var profilepic = "";
var lnkcmpurl = "";
var email = [];
var companyLinkedInUrl = "";
var companyWebsiteUrl = "";
var angleCompanyUrl = "";
var linkedinurl = "";
var fburl = "";
var twitterurl = "";
var profileurl = "";


var liAccount;

var isRecruterIntP = false;
var isSalesNavIntP = false;
var isDefIntP = false;

var person = {};
var maDefaultListId = 0;

var $select = $('#userPeopleSelect');
var countAttemptsGetEmail = 0;
var maxCountAttemptsGetEmail = 10;

var csrfToken = false; 

var extractedData = {};
var e_e;


var getSingleProfileLinkedinData = function () {

    var profileName = "";
    var companyName = "";
    var colgName = "";
    var userLocation = "";
    var designation = "";
    var profileurl = "";
    var companyWebsiteUrl = "";
    var profilesection = "";

    // const elements = []
    profilesection = $("#profile-content").html();
    
    const regexName = /t-24/g;
    let m;
    while ((m = regexName.exec(profilesection)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regexName.lastIndex) {
            regexName.lastIndex++;
        }
        
        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
            console.log(`Found match, group ${groupIndex}: ${match}`);
            console.log(match);
            try {
                // profileName = document.getElementsByClassName("pv-top-card-section__name inline t-24 t-black t-normal")[0].innerHTML.trim();
                profileName = $("."+match).text().trim();
            } catch (e) {
                profileName = "";
            }
        });
    }

    const regexLocation = /t-16 t-black t-normal inline-block/g;
    while ((m = regexLocation.exec(profilesection)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regexLocation.lastIndex) {
            regexLocation.lastIndex++;
        }
        
        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
            console.log(`Found match, group ${groupIndex}: ${match}`);
            console.log(match);
            try {
                // userLocation = document.getElementsByClassName("pv-top-card-section__location")[0].innerHTML.trim();
                userLocation = $("."+match).text().trim();
            } catch (e) {
                userLocation = "";
            }
        });
    }


    const regexPosition = /class="t-16 t-black t-normal inline-block"/g;
    while ((m = regexPosition.exec(profilesection)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regexPosition.lastIndex) {
            regexPosition.lastIndex++;
        }
        
        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
            console.log(`Found match, group ${groupIndex}: ${match}`);
            console.log(match);
            try {
                // userLocation = document.getElementsByClassName("pv-top-card-section__location")[0].innerHTML.trim();
                userLocation = $("."+match).text().trim();
            } catch (e) {
                userLocation = "";
            }
        });
    }
    
    // const findTag = /<[^\/].*?>/g
    // let element
    // while(element = findTag.exec(profilesection)) {
    //     element = element[0];
    //     const classes = (element.match(/class="(.*?)"/i) || [,""])[1].split(' ')
    //     element = {}
    //     element["class"]= classes
    //     elements.push(element)
    // }
    
    // console.log(element);
    // // console.log(result)

    
    

    profileurl = window.location.href;
    
    try {
        // companyName = document.getElementsByClassName("pv-top-card-v2-section__entity-name pv-top-card-v2-section__company-name text-align-left ml2 t-14 t-black t-bold lt-line-clamp lt-line-clamp--multi-line ember-view")[0].innerText.trim();
        companyName = $('.pv-entity__secondary-title:first').text().trim();
    } catch (err) {
        companyName = "";
    }
    try {
        // colgName = document.getElementsByClassName("pv-top-card-v2-section__entity-name pv-top-card-v2-section__school-name text-align-left ml2 t-14 t-black t-bold lt-line-clamp lt-line-clamp--multi-line ember-view")[0].innerText.trim();
        colgName = $('.pv-entity__school-name:first').text().trim();
    } catch (err) {
        colgName = "";
    }
    
    try {
        // designation = document.getElementsByClassName("pv-top-card-section__headline mt1 t-18 t-black t-normal")[0].innerText.trim();
        designation = $('#experience-section ul div:nth-child(1) .pv-profile-section__card-item-v2 h3').text().trim();
    } catch (e) {
        designation = "";
    }
    // profile image source from linkedin
    var profilepic = "";
    try {
        // var imgUrlTemp = document.getElementsByClassName("pv-top-card-section__photo presence-entity__image EntityPhoto-circle-9 ember-view")[0].style.backgroundImage.trim();
        // profilepic = imgUrlTemp.substring(imgUrlTemp.indexOf("https://"), imgUrlTemp.indexOf("\")"));

        profilepic = $('.pv-top-card-section__photo-wrapper img').attr('src');
    } catch (e) {
        profilepic = "";
    }
    companyProfileId = $('.pv-profile-section__sortable-item a.ember-view').attr('href');
    console.log(companyProfileId);

    companyLinkedInUrl = getCompanyLinkedInUrl();
    if (document.getElementById("companyProfileIframe" + 1)) {
        var elem = document.getElementById("companyProfileIframe1");
        elem.parentNode.removeChild(elem);
    }

    

    localStorage.setItem('companyWebsiteUrl', '');
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("id", "companyProfileIframe" + 1);
    iframe.setAttribute("src", companyLinkedInUrl);
    iframe.style.visibility = 'hidden'; //hidden
    document.body.appendChild(iframe);
    inDapIF = true;
    document.getElementById("companyProfileIframe" + 1).onload = function () {
        localStorage.setItem('companyLinkedInUrl', '');
        var companyProfileIframe = document.getElementById("companyProfileIframe" + 1);
            companyProfileIframe.contentWindow.scrollTo(0, 1200);
            var innerDoc = companyProfileIframe.contentDocument || companyProfileIframe.contentWindow.document;
            var companyWebsiteUrl = innerDoc.getElementsByClassName('org-top-card-primary-actions__action ember-view')[1].href;
            chrome.storage.sync.set({extCompanyWebsiteUrl: companyWebsiteUrl}, function () {
            });
    };
    var iframe = document.createElement("IFRAME");

    


    return {
        "name": profileName,
        "college": colgName,
        "company": companyName,
        "position": designation,
        "mode": "LINKEDIN",
        "profileurl": profileurl,
        "profilepic": profilepic,
        "facebookurl": "",
        "twitterurl": "",
        "linkedinurl": "",
        "email": email,
        "userLocation:": userLocation,
        "companyWebsiteUrl": companyWebsiteUrl,
    };
};

// get single search data from Linkedin sales navigator
var getSingleProfileLinkedinNavigator = function () {
    var websiteName = window.location.hostname;
    var frListSearch = window.location.pathname;

    var profileName = "";
    var position = "";
    var companyName = "";
    var colgName = "";
    var profileUrl = "";
    var imgUrl = "";
    var companyLinkedInUrl = "";

    try {
        // profileName = document.getElementsByClassName("profile-topcard-person-entity__name")[0].innerText;
        profileName = $('.profile-topcard-person-entity__name').text().trim();
    } catch (err) {
        profileName = "";
    }
    try {
        position = document.getElementsByClassName("profile-position__title")[0].innerText;
        position = $(".profile-position__title:first").text().trim();

    } catch (err) {
        position = "";
    }
    try {
        // companyName = document.getElementsByClassName("profile-position__secondary-title")[0].getElementsByClassName("ember-view")[0].innerText;
        companyName = $('.profile-position__secondary-title:first a').text().trim();

    } catch (err) {
        companyName = "";
    }
    try {
        colgName = $('.profile-education__school-name').text().trim();
    } catch (err) {
        colgName = "";
    }
    try {
        imgUrl = $(".profile-topcard-person-entity__image img").attr('src');
    } catch (err) {
        imgUrl = "";
    }

    // important fields handle in catch also
    try {
        profileUrl = websiteName + frListSearch;
    } catch (err) {
        profileUrl = websiteName + frListSearch;
    }
    companyLinkedInUrl = $('.profile-position__secondary-title:first a').attr('href');
    //initalize variables
    // localStorage.setItem('companyWebsiteUrl', '');

    if (document.getElementById("companyProfileIframe" + 1)) {
        var elem = document.getElementById("companyProfileIframe1");
        elem.parentNode.removeChild(elem);
    }
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("id", "companyProfileIframe" + 1);
    iframe.setAttribute("class", "newFrame");
    iframe.setAttribute("src", companyLinkedInUrl);
    iframe.style.visibility = 'hidden'; //hidden
    document.body.appendChild(iframe);
    document.getElementById("companyProfileIframe1").onload = function () {
        localStorage.setItem('companyLinkedInUrl', '');
        var companyProfileIframe = document.getElementById("companyProfileIframe1");
        setTimeout(function () {
            companyProfileIframe.contentWindow.scrollTo(0, 1200);
            var innerDoc = companyProfileIframe.contentDocument || companyProfileIframe.contentWindow.document;
            console.log("innerDoc:   ",innerDoc);
            var companyWebsiteUrl = '';
       
            localStorage.setItem('companyWebsiteUrl', companyWebsiteUrl);
            localStorage.setItem('companyLinkedInUrl', companyLinkedInUrl);
            localStorage.setItem('companyname', companyName);
        }, 100);

        //get companyWebsiteUrl from localStorage.
        companyWebsiteUrlInterval = setInterval(function () {
            companyWebsiteUrl = localStorage.getItem('companyWebsiteUrl');
            if (companyWebsiteUrl !== '') {
                clearInterval(companyWebsiteUrlInterval);
                chrome.storage.sync.set({extCompanyWebsiteUrl: companyWebsiteUrl}, function () {
                });
            }
        }, 100);
    };

    return {
        "name": profileName,
        "college": colgName,
        "company": companyName,
        "position": position,
        "mode": "LINKEDIN",
        "profileurl": profileUrl,
        "profilepic": imgUrl,
        "email": "",
        "companyWebsiteUrl": companyWebsiteUrl,
        "facebookurl": "",
        "twitterurl": "",
        "linkedinurl": ""
    };
};

var navigatorUrlList = [];
var navigatorUserList = [];
var getAllLinksInNavigatorPage = function () {
    var totalProfiles = document.getElementsByClassName("result-lockup__name");

    for (var j = 0; j < totalProfiles.length; j++) {
        var profileNm = document.getElementsByClassName("result-lockup__name")[j].innerText
        var profileUrl = document.getElementsByClassName("result-lockup__name")[j].getElementsByClassName('ember-view')[0].href;
        navigatorUserList.push(profileNm);
        navigatorUrlList.push(profileUrl);
        totalProfile++;
        chrome.storage.sync.set({'totalProfile': totalProfile}, function () {
        });
        localStorage.setItem("totalProfile", JSON.stringify({totalProfile: totalProfile}));
    }
};

// default function
function getData() {
    clear();
    var websiteName = window.location.hostname;
    var frListSearch = window.location.pathname;
    // linkedin data extract
    if (websiteName === "www.linkedin.com" && !frListSearch.match("/sales/profile") && !frListSearch.match("/sales/search")) {
        if (frListSearch === '/search/results/people/') {
            setTimeout(function () {
                window.scrollTo(5, 700);
            }, 7000);

            setTimeout(function () {
                getAllLinksInPage();
                localStorage.setItem('companyLinkedInUrl', '');
                chrome.storage.sync.set({extCompanyWebsiteUrl: companyLinkedInUrl}, function () {
                });
                var k = 0;
                if (activeResumeMode) {
                    var localLastCount = JSON.parse(localStorage.getItem("lastCount"));
                    k = localLastCount.lastCount;
                    createIFrameAndGetMetaData(k);
                } else {
                    // set initial value of k to start
                    k = 0;
                    createIFrameAndGetMetaData(k);
                }
            }, 8000);
            var div = document.getElementsByTagName("BODY")[0];
            div.style.pointerEvents = 'none';
            $(window).click(function () {
                toastr.error("User interaction disabled while Alore Email finder is working", "Alore Email Finder");
            });

        } else {
            var obj = {};
            obj = getSingleProfileLinkedinData();
            console.log('obj :', obj);
            chrome.storage.sync.set({extractedObj: obj}, function () {
            });
            chrome.storage.sync.get('extractedObj', function (extractedObj) {
                var date = new Date();
                var timestamp = date.getTime();
            });
        }
    }

    if (websiteName === "www.linkedin.com" && (frListSearch.match("/sales/profile") || frListSearch.match("/sales/people") )) {
        // for Linkedin Sales Navigator
        obj = getSingleProfileLinkedinNavigator();
        chrome.storage.sync.set({extractedObj: obj}, function () {
        });
    }

    if (websiteName === "www.linkedin.com" && frListSearch.match("/sales/search")) {
        setTimeout(function () {
            getAllLinksInNavigatorPage();
            var n = 0;
            if (activeResumeMode) {
                var localLastCount = JSON.parse(localStorage.getItem("lastCount"));
                n = localLastCount.lastCount;
                createIFrameAndGetNavigatorMetaData(n);
            } else {
                n = 0;
                createIFrameAndGetNavigatorMetaData(n);
            }
            var div = document.getElementsByTagName("BODY")[0];
            div.style.pointerEvents = 'none';
            $(window).click(function () {
                toastr.error("User interaction disabled while Alore Email finder is working", "Alore Email Finder");
            });
        }, 5000);
    }
}

getData();

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var saveProfileData = function (exData, profileName) {
    var url = "https://crmapp.alore.io:8080/socialdatasave/save";
    var toSendObj = {};

    toSendObj.userid = userid;
    toSendObj.companyid = companyid;
    toSendObj.name = exData.name;
    toSendObj.college = exData.college;
    toSendObj.company = exData.company;
    toSendObj.position = exData.position;
    toSendObj.mode = exData.mode;
    toSendObj.profileurl = exData.profileurl;
    toSendObj.profilepic = exData.profilepic;
    toSendObj.email = "";
    toSendObj.companyWebsiteUrl = exData.companyWebsiteUrl;
    console.log(exData.companyWebsiteUrl);
    toSendObj.facebookurl = exData.facebookurl;
    toSendObj.twitterurl = exData.twitterurl;
    toSendObj.linkedinurl = exData.linkedinurl;
    toSendObj.tag = tag;
    var http = new XMLHttpRequest();
    http.open("POST", url, true);
    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/json");
    http.onreadystatechange = function () {
        //Call a function when the state changes.
        if (http.readyState === 4 && http.status === 200) {
            toastr.success("Found email and saved successfully for '" + profileName + "'");
            profileSuccess++;
            chrome.storage.sync.set({'profileSuccess': profileSuccess}, function () {
            });
            localStorage.setItem("profileSuccess", JSON.stringify({profileSuccess: profileSuccess}));
        } else if (http.readyState === 4 && http.status === 400) {
            toastr.error("Contact already exist for '" + profileName + "'");
            profileExist++;
            chrome.storage.sync.set({'profileExist': profileExist}, function () {
            });
            localStorage.setItem("profileExist", JSON.stringify({profileExist: profileExist}));
        } else if (http.readyState === 4 && http.status === 402) {
            toastr.error("Couldn't found email for '" + profileName + "'");
            skippedProfile++;
            chrome.storage.sync.set({'skipCount': skippedProfile}, function () {
            });
            localStorage.setItem("skipCount", JSON.stringify({skipCount: skippedProfile}));
        } else if (http.readyState === 4 && http.status === 404) {
            toastr.error("Problem in connecting to server. Please check your internet connection");
            // chrome.runtime.sendMessage({
            //     action: 'showError',
            //     value: 'Problem in connecting to server. Please check your internet connection'
            // });
            skippedProfile++;
            chrome.storage.sync.set({'skipCount': skippedProfile}, function () {
            });
        } else if (http.readyState === 4 && http.status === 420) {
            toastr.error("Couldn't found domain name for '" + profileName + "'");
            skippedProfile++;
            chrome.storage.sync.set({'skipCount': skippedProfile}, function () {
            });
        } else if (http.readyState === 4 && http.status === 421) {
            toastr.error("Email credits not available. Please Buy or Refer to get more credits.", "Alore Email Finder");
            // chrome.runtime.sendMessage({
            //     action: 'showError',
            //     value: 'Email credits not available. Please Buy or Refer to get more credits.'
            // });
            skippedProfile++;
            chrome.storage.sync.set({'skipCount': skippedProfile}, function () {
            });
        } else if (http.readyState === 4 && http.status === 422) {
            toastr.error("Oops!Something went wrong. Please Contact CRM Support.", "Alore Email Finder");
            // chrome.runtime.sendMessage({
            //     action: 'showError',
            //     value: 'Oops!Something went wrong. Please Contact CRM Support.'
            // });
            skippedProfile++;
            chrome.storage.sync.set({'skipCount': skippedProfile}, function () {
            });
        } else if (http.readyState === 4 && http.status === 500) {
            toastr.error("Oops!Something went wrong. Please try again.", "Alore Email Finder");
            // chrome.runtime.sendMessage({
            //     action: 'showError',
            //     value: 'Oops!Something went wrong. Please Contact CRM Support.'
            // });
            skippedProfile++;
            chrome.storage.sync.set({'skipCount': skippedProfile}, function () {
            });
        } else if (http.status === -1 || http.status <= 0) {
            toastr.warning("Connection lost please restart session.", "Alore Email Finder");
            // chrome.runtime.sendMessage({
            //     action: 'showError',
            //     value: 'Connection lost please restart session.'
            // });
            skippedProfile++;
            chrome.storage.sync.set({'skipCount': skippedProfile}, function () {
            });
        }
    };
    http.send(JSON.stringify(toSendObj));
};


angular.module('alore.factory', [])
    .factory('aloreFactory', ['$rootScope', '$http', function ($rootScope, $http) {
        return {
            crmLogin: function (compDetails, loginmode) {
                var jsonToSend = {
                    "email": compDetails.compEmail,
                    "password": compDetails.compPass,
                    "loginmode": loginmode
                };
                return $http({method: 'POST', url: ssoBaseUrl + "logincontroller/login", data: jsonToSend});
            },
            profileExtractorDataSave: function (exData, usrObj) {
                var jsonToSend = {
                    "companyid": usrObj.companyId,
                    "userid": usrObj.userId,
                    "name": exData.name,
                    "college": exData.college,
                    "company": exData.company,
                    "position": exData.position,
                    "mode": exData.mode,
                    "profileurl": exData.profileurl,
                    "profilepic": exData.profilepic,
                    "email": "",
                    "companyWebsiteUrl": exData.companyWebsiteUrl,
                    "facebookurl": exData.facebookurl,
                    "twitterurl": exData.twitterurl,
                    "linkedinurl": exData.linkedinurl,
                    "tag":exData.tag,
                };
                return $http({method: 'POST', url: urlEndPoint + 'save', data: jsonToSend});
            },

            riseSupportForFailure: function (usrObj) {
                var jsonToSend = {"companyid": usrObj.companyId, "userid": usrObj.userId};
                return $http({method: 'POST', url: urlEndPoint + 'risesupport', data: jsonToSend});
            },
            getonloaddata: function (usrObj, version) {
                var jsonToSend = {"companyid": usrObj.companyId, "userid": usrObj.userId, "version": version};
                return $http({method: 'POST', url: baseUrl + 'token/getonloaddata', data: jsonToSend});
            },
            saveExtensionSettings: function (usrObj, settingObj) {
                var jsonToSend = {"companyid": usrObj.companyId, "userid": usrObj.userId, "dailyCounts": settingObj.dailyCounts,"hourlyCount":settingObj.hourlyCount,"linkedinAccountType":settingObj.linkedinAccountType};
                return $http({method: 'POST', url: baseUrl + 'token/savesettings', data: jsonToSend});
            },

            tokenValidationCall: function (usrObj, mode) {
                var jsonToSend = {
                    "companyid": usrObj.companyId,
                    "userid": usrObj.userId,
                    "token": usrObj.token,
                    "loginmode": mode
                };
                return $http({method: 'POST', url: baseUrl + "token", data: jsonToSend});
            },
            versionCheck: function (usrObj, version) {
                var jsonToSend = {
                    "companyid": usrObj.companyId,
                    "userid": usrObj.userId,
                    "token": usrObj.token,
                    "version": version
                };
                return $http({method: 'POST', url: baseUrl + "token/extensionversion", data: jsonToSend});
            },
            getAllTags: function (usrObj) {
                var jsonToSend = {
                    "companyid": usrObj.companyId,
                    "userid": usrObj.userId,
                };
                return $http({method: 'POST', url: baseUrl + "tags/getall", data: jsonToSend});
            },
        };
    }]);
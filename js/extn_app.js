/**
* aloreExtn Module
*
* Description
*/

var urlEndPoint = "https://crmapp.alore.io:8080/socialdatasave/";
var baseUrl = "https://crmapp.alore.io:8080/";
var ssoBaseUrl = "https://accounts.alore.io:8081/";
var version = "2.0.0.7";

var exTn = angular.module('aloreExtn', ['ui.router', "alore.controllers", "alore.factory", "ngFileUpload"])
	.config(Config);
Config.$inject = ['$urlRouterProvider', '$stateProvider'];
function Config($urlRouterProvider, $stateProvider) {
	//$urlRouterProvider.otherwise('/login');
	$stateProvider
		.state("prospectList", {
			url: "/prospectList",
			controller: "extractProspectSocialCtrl",
			templateUrl: "extractProspectSocial.html"
		})
		.state("login", {
			url: "/login",
			controller: "loginCtrl",
			templateUrl: "login.html"
		});
}
exTn.config(['$compileProvider', function ($compileProvider) {
	$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
}]);
exTn.run(function ($rootScope, $state) {
	var isTkn = localStorage.getItem("usrCompObj");
	if (isTkn) {
		$state.go("prospectList");
	} else {
		$state.go("login");
	}
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    //here we get the new
    //console.log("URL CHANGED: " + request.data.url);
});

chrome.tabs.onUpdated.addListener(function(tabId,changeInfo,tab){
    if (tab.url.indexOf("https://angel.co/search") > -1 &&
        changeInfo.url === undefined){
        localStorage.setItem("aloreObj", JSON.stringify({workingMode: 0}));
    }
});

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    console.log('chrome inserted first');
    if (msg.action === "updateIcon") {
        if (msg.value) {
            chrome.browserAction.setIcon({path: "img/Alore_ Logo 16_16.png"});
        } else {
            chrome.browserAction.setIcon({path: "img/logo_Alore_Blue_16x16.png"});
        }
    }else if (msg.action === "showError") {
        console.log('chrome inserted');
        var opt = {
            iconUrl : 'img/logo_Alore_Blue_16x16.png',
            type: 'basic',
            title: "Email Finder",
            message: "Primary message to display",
            priority: 2,
            requireInteraction : true
        };
        chrome.notifications.create('notify1', opt, function () {
            console.log('created!');
        });
    }
});



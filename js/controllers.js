angular.module('alore.controllers', ["alore.factory"])
    .controller('extractProspectSocialCtrl', ['$scope', "$rootScope", "aloreFactory", "$state", "Upload", "$timeout", "$window",
        function ($scope, $rootScope, aloreFactory, $state, Upload, $timeout, $window) {

            var frmLocal = JSON.parse(localStorage.getItem("usrCompObj"));
            $scope.aloreObj = {};
            $scope.settingObj = {};
            $scope.aloreObj.workingMode = JSON.parse(localStorage.getItem("aloreObj")).workingMode;
            $rootScope.extnLoader = true;
            $scope.version = version;
            $scope.extractedData = {};
            $scope.currentWorkingProfile = 0;
            $scope.progressProfileName = '';
            var progressBarObj = {};

            /* Default Variable */

            $scope.showClickHere        = false;
            $scope.showLoginButton      = false;
            $scope.stateLoader          = false;
            $scope.settingInfo          = false;
            $scope.accountTypeChange    = false;
            $scope.perDaySearch         = false;
            $scope.perHourSearch        = false;
            $scope.searchMediaPage      = false;
            $scope.loader               = false;
            $scope.disableBtn           = false;
            $scope.viewProfileMode      = false;
            $scope.multiSearch          = false;
            $scope.selfProfileError     = false;
            $scope.enoughCreditScore    = false;
            $scope.chooseFilterForMultiPro = false;
            $scope.allSettingSave       = true;
            $scope.linkedinSettingsPage = true;
            $scope.showSearchExtractSettings = true;

            $scope.closeAllTabs = function () {
                $scope.linkedinSettingsPage = false;
                $scope.showSearchExtractSettings = false;
                $scope.showSingleSearch = false;
                $scope.searchMediaPage = false;
                $scope.stateLoader = false;
                $rootScope.extnLoader = false;
                $rootScope.loadergif = false;
                $scope.chooseFilterForMultiPro = false;
            };

            /* For Internet Connectivity */
            if (!navigator.onLine) {
                $scope.showSearchExtractSettings = false;
                $rootScope.loader = false;
                // $scope.succMsg2 = false;
                $scope.succMsg5 = true;
                $scope.succMsg = false;
                $scope.extnSuccessMsg4 = "Unable to connect server. Please check your internet connection.";
            }
            // clear();
            tempTimeout = 0;
            tempProfileNum = 0;
            $scope.skipCount = 0;
            $scope.totalProfile = 0;
            $scope.profileSuccess = 0;
            $scope.profileExist = 0;
            chrome.storage.sync.get('skipCount', function (obj) {
                if (obj.skipCount) {
                    $scope.skipCount = obj.skipCount;
                }
            });
            
            chrome.storage.sync.get('totalProfile', function (obj) {
                if (obj.totalProfile) {
                    $scope.totalProfile = obj.totalProfile;
                }
            });

            chrome.storage.sync.get('profileSuccess', function (obj) {
                if (obj.profileSuccess) {
                    $scope.profileSuccess = obj.profileSuccess;
                }
            });
            chrome.storage.sync.get('profileExist', function (obj) {
                if (obj.profileExist) {
                    $scope.profileExist = obj.profileExist;
                }
            });

            chrome.storage.sync.get('currentProfile', function (obj) {
                if (obj.currentProfile) {
                    $scope.currentWorkingProfile = obj.currentWorkingProfile;
                }
            });

            setInterval(function () {
                var options = {
                    classname: 'progress-bar',
                    id: 'progress-bar',
                    target: document.getElementById('progressbar-subwrap')
                };
                if(!document.getElementById('progress-bar')){
                    nanobar = new Nanobar(options);
                }
                chrome.storage.sync.get('skipCount', function (obj) {
                    if (obj.skipCount) {
                        $scope.skipCount = obj.skipCount;
                    }
                });
                chrome.storage.sync.get('totalProfile', function (obj) {
                    if (obj.totalProfile) {
                        $scope.totalProfile = obj.totalProfile;
                    }
                });

                chrome.storage.sync.get('profileSuccess', function (obj) {
                    if (obj.profileSuccess) {
                        $scope.profileSuccess = obj.profileSuccess;
                    }
                });
                chrome.storage.sync.get('profileExist', function (obj) {
                    if (obj.profileExist) {
                        $scope.profileExist = obj.profileExist;
                    }
                });


                chrome.storage.sync.get('LinkedinProgresbarTimeout', function (tempObj) {
                    if(tempObj && tempObj.LinkedinProgresbarTimeout){
                        progressBarObj = tempObj.LinkedinProgresbarTimeout;
                    }
                });
                chrome.storage.sync.get('LinkedinNaviProgresbarTimeout', function (tempObj) {
                    if(tempObj.LinkedinNaviProgresbarTimeout){
                        progressBarObj = tempObj.LinkedinNaviProgresbarTimeout;
                    }
                });
                chrome.storage.sync.get('TwitterProgresbarTimeout', function (tempObj) {
                    if(tempObj.TwitterProgresbarTimeout){
                        progressBarObj = tempObj.TwitterProgresbarTimeout;
                    }
                });
                chrome.storage.sync.get('AngelProgresbarTimeout', function (tempObj) {
                    if(tempObj.AngelProgresbarTimeout){
                        progressBarObj = tempObj.AngelProgresbarTimeout;
                    }
                });

                console.log("progressbarobj", progressBarObj);

                if (progressBarObj && progressBarObj.timeout) {
                    if (progressBarObj.profileNum !== undefined && progressBarObj.percent !== undefined && progressBarObj.timeout !== undefined) {
                        $scope.loader = false;
                        var progressTimeout = parseInt(parseInt(progressBarObj.timeout)/2);
                        var progressPercent = parseInt(progressBarObj.percent);
                        var progressProfileNum = parseInt(progressBarObj.profileNum);
                        var progressProfileName = progressBarObj.profileName;
                        $scope.progressProfileName = progressProfileName;
                        $scope.currentWorkingProfile = progressProfileNum + 1;
                        if (tempProfileNum !== progressProfileNum && progressProfileNum != 0) {
                            tempProfileNum = progressBarObj.profileNum;
                            /* **************** Progress Bar ***************** */
                            /* **************** Progress Bar ***************** */
                            nanobar.go(0);
                        } else {
                            if (tempTimeout !== progressTimeout) {
                                tempTimeout = progressTimeout;

                                setTimeout(function () {
                                    nanobar.go(progressPercent);
                                }, progressTimeout);
                            }
                        }
                    }else {
                        $scope.loader = true;
                    }
                }
            }, 100);


            /* Initiate values in chrome storage */
            chrome.storage.sync.set({'userid': frmLocal.userId}, function () {});

            chrome.storage.sync.set({'companyid': frmLocal.companyId}, function () {});

            /* Check current version of Extension */
            $scope.versionChecker = function () {
                aloreFactory.versionCheck(frmLocal, version)
                    .then(function (res) {
                        $scope.logedInEmail = res.data.message;
                    }, function (res) {
                        $scope.closeAllTabs();
                        $scope.succMsg = true;
                        if (res.data.code === 400) {
                            $scope.extnSuccessMsg3 = "Please update to the newest version of Alore Email Finder to continue searching.";
                            $scope.showClickHere = true;
                        }
                    });
            };
            $scope.versionChecker();
            /* get new version of Extension */
            $scope.gotoNewVersionLink = function () {
                window.open("https://chrome.google.com/webstore/detail/alore-email-finder/lbgklopkcngaanlkphmjoomdhagnaapo", '_blank')
            };

            var mode = 2;
            /* To check linkedin account url and linkeding premium account(navigator) url */
            $scope.checkSeachBox = function () {
                $scope.stateLoader = true;
                chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
                    var linkedInUrl = 'https://www.linkedin.com/search/results/people/';
                    var navigatorUrl = "https://www.linkedin.com/sales/search/people";
                    var ownProfile  = "https://www.linkedin.com/feed/";
                    var multiWithoutFilter = tabs[0].url.split("?")[1];
                    var tan = tabs[0].url.split("?")[0];
                    if(tabs[0].url === ownProfile){
                        /* Signle own Linkedin Profile */
                        $scope.loader = false;
                        $scope.closeAllTabs();
                        $scope.searchMediaPage = false;
                        $scope.linkedinSettingsPage = false;
                        $scope.selfProfileError = true;
                    }
                    else if (tabs[0].url.split("?")[0] !== linkedInUrl && getParameterByName('f', tabs[0].url) !== "users" && getParameterByName('type', tabs[0].url) !== "people" && tabs[0].url.split("?")[0] !== navigatorUrl) {
                        /* Signle Linkedin Profile */
                        $scope.closeAllTabs();
                        $scope.extractedData.name = "";
                        $scope.extractedData.position = "";
                        $scope.extractedData.companyWebsiteUrl = "";
                        $scope.extractedData.profileurl = "";
                        $scope.showSingleSearch = true;
                        $scope.loadonstartup();
                    } else {
                        /* Multiple Linkedin Profile Without Filter */
                        if(multiWithoutFilter.indexOf("origin") === 0){
                            $scope.closeAllTabs();
                            $scope.extractedData.name = "";
                            $scope.extractedData.position = "";
                            $scope.extractedData.companyWebsiteUrl = "";
                            $scope.extractedData.profileurl = "";
                            $scope.chooseFilterForMultiPro = true;
                            $scope.searchMediaPage = false;
                            $scope.linkedinSettingsPage = false;
                        }else{
                            /* Multiple Linkedin Profile */
                            $scope.closeAllTabs();
                            $scope.loader = false;
                            $scope.multiSearch = true;
                            $scope.showSearchExtractSettings = true;
                        }
                    }
                });
                // });
            };
            $scope.checkSeachBox();

            /* Calculate the number of emails searched per session */
            $scope.getprofileSuccessFromDB = function () {
                chrome.storage.sync.get('profileSuccess', function (arm) {
                    $scope.profileSuccesssync = arm['profileSuccess'];
                    if (!$scope.profileSuccesssync){
                        $scope.profileSuccesssync = 0;
                    }
                });
            };
            $scope.getprofileSuccessFromDB();

            /* Start from here */
            $scope.getOnLoadData = function () {
                aloreFactory.getonloaddata(frmLocal, version)
                    .then(function (res) {
                        // if(res.data.extensionDetails){
                        if (res.data.extensionDetails) {
                            if (Object.keys(res.data.extensionDetails).length === 0) {
                                $scope.settingInfo = false;
                                $scope.searchMediaPage = false;
                                $scope.linkedinSettingsPage = true;
                            } else {
                                $scope.settingInfo = true;
                                if ($scope.chooseFilterForMultiPro === false && $scope.selfProfileError === false && $scope.enoughCreditScore === false) {
                                    $scope.searchMediaPage = true;
                                }
                                $scope.emailCredits = res.data.emailCredits

                                if ($scope.emailCredits === 0) {
                                    $scope.closeAllTabs();
                                    $scope.enoughCreditScore = true;
                                    $scope.searchMediaPage = false;
                                    $scope.linkedinSettingsPage = false;

                                }
                                $scope.settingObj.hourlyCount = res.data.extensionDetails.hourlycount;
                                $scope.settingObj.dailyCounts = res.data.extensionDetails.dailycounts;
                                $scope.settingObj.linkedinAccountType = res.data.extensionDetails.linkedinaccounttype;
                                chrome.storage.sync.set({'settingsObj': $scope.settingObj}, function () {
                                });
                                $scope.settingOptionChange();
                            }
                        }
                    }, function (res) {

                    });
            };
            $scope.getOnLoadData();

            $scope.loader = true;
            $scope.loadonstartup = function () {
                chrome.tabs.executeScript({file: "/library/jquery.min.js"}, function (result) {});

                chrome.tabs.insertCSS({file: "css/toastr.min.css"}, function (result) {});

                chrome.tabs.executeScript({file: "js/toastr.min.js"}, function (result) {});

                chrome.tabs.executeScript({file: "js/mulScript.js"}, function (result) {});

                var resultUrl = "";
                setTimeout(function () {
                    $scope.extractedData = {};
                    chrome.storage.sync.get('extractedObj', function (obj) {
                        var date = new Date();
                        var timestamp = date.getTime();

                        $scope.tempObj = obj['extractedObj'];
                        $scope.extractedData = $scope.tempObj;
                        console.log('extractedData :',$scope.extractedData);
                        $scope.loader = false;
                        if (!$scope.extractedData || $scope.extractedData === null || $scope.extractedData === undefined) {
                            function clear() {
                                chrome.storage.sync.clear(function (obj) {
                                });
                            }
                            clear();
                            clearInterval(companyWebsiteUrlInterval);
                            $scope.loader = false;
                            $scope.succMsg = true;
                            $scope.showClickHere = false;
                            $scope.extnSuccessMsg3 = "I can share information only when you view profile. Please search peoples on LinkedIn, AngelList or Twitter.";
                            $timeout(function () {
                                $scope.extnSuccessMsg = "";
                            }, 100);
                        }else{
                            $scope.loader = false;
                            $scope.showSingleSearch = true;
                        }
                    });
                },500);

                chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
                    var domainUrl = tabs[0].url.split("?")[0];
                    resultUrl = extractHostname(domainUrl);
                    if (resultUrl != "www.linkedin.com" && resultUrl != "angel.co" && resultUrl != "twitter.com") {
                        clearInterval(companyWebsiteUrlInterval);
                        $scope.loader = false;
                        $scope.succMsg = true;
                        $scope.showClickHere = false;
                        $scope.extnSuccessMsg3 = "I can share information only when you view profile. Please search peoples on LinkedIn, AngelList or Twitter.";
                        // $timeout(function () {
                        //     $scope.extnSuccessMsg = "";
                        // }, 5000);
                    }else if (resultUrl == "www.linkedin.com" && domainUrl.indexOf("search") != -1 && domainUrl != "https://www.linkedin.com/search/results/people" && tabs[0].url == "https://www.linkedin.com/search/results/people"){
                        var originUrl = tabs[0].url.split("?")[1];
                        if (originUrl == undefined){
                            originUrl = "origin=DISCOVER_FROM_SEARCH_HOME";
                        }
                        var finalUrl = "https://www.linkedin.com/search/results/people?"+originUrl;
                        window.open(finalUrl, "_blank");
                    }else if (resultUrl == "angel.co" && tabs[0].url.split("?")[0] == "https://angel.co/search" && tabs[0].url.split("&type=")[1] != "people"){
                        var spUrl = tabs[0].url+"&type=people";
                    }else if (resultUrl == "twitter.com" && tabs[0].url.split("?")[0] == "https://twitter.com/search" && getParameterByName('f') != "users"){
                        var spStr = tabs[0].url.split("&vertical=default")[1];
                        var toOpenUrl = domainUrl + "?f=users&vertical=default"+spStr;
                        window.open(toOpenUrl, "_blank");
                    }
                });
            };

            /* Enable submit button for setting option only if all the 3 parameter are set */
            $scope.settingOptionChange = function(){
                if($scope.settingObj.linkedinAccountType!== undefined && $scope.settingObj.dailyCounts!== undefined && $scope.settingObj.hourlyCount!== undefined){
                    $scope.allSettingSave = false;
                }
            };

            // test function for search result tst
            $scope.seachDataMode = {};

            /* Start Extracting Data From Profile */
            $scope.startExtractingData = function () {
                $scope.showSearchExtractSettings = true;
                $scope.showSingleSearch = true;
                $scope.linkedinSettingsPage = false;
                localStorage.setItem("aloreObj", JSON.stringify({workingMode: 1}));
                $scope.aloreObj.workingMode = JSON.parse(localStorage.getItem("aloreObj")).workingMode;
                $scope.loadonstartup();
                chrome.runtime.sendMessage({
                    action: 'updateIcon',
                    value: true
                });
                window.close();
            };

            /* Stop Extracting Data From Profile */
            $scope.stopExtractingData = function () {
                localStorage.setItem("aloreObj", JSON.stringify({workingMode: 0}));
                $scope.aloreObj.workingMode = JSON.parse(localStorage.getItem("aloreObj")).workingMode;
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {chrome.tabs.reload(tabs[0].id);});
                chrome.storage.sync.set({'activeResumeMode': false}, function(){});
                chrome.storage.sync.set({'LinkedinProgresbarTimeout':{}},function (obj) {});
                chrome.storage.sync.set({'AngelProgresbarTimeout':{}},function (obj) {});
                chrome.storage.sync.set({'TwitterProgresbarTimeout':{}},function (obj) {});
                chrome.storage.sync.set({'LinkedinNaviProgresbarTimeout':{}},function (obj) {});
                chrome.runtime.sendMessage({
                    action: 'updateIcon',
                    value: false
                });
            };

            /* Pause Extracting Data From Profile */
            $scope.pauseExtractingData = function () {
                localStorage.setItem("aloreObj", JSON.stringify({workingMode: 2}));
                $scope.aloreObj.workingMode = JSON.parse(localStorage.getItem("aloreObj")).workingMode;
                chrome.storage.sync.set({'pauseEnable': true}, function () {
                });
                chrome.storage.sync.set({'activeResumeMode': false}, function () {
                });
                chrome.runtime.sendMessage({
                    action: 'updateIcon',
                    value: false
                });
            };

            /* Resume Extracting Data From Profile */
            $scope.resumeExtractingData = function () {
                localStorage.setItem("aloreObj", JSON.stringify({workingMode: 1}));
                $scope.aloreObj.workingMode = JSON.parse(localStorage.getItem("aloreObj")).workingMode;
                chrome.storage.sync.set({'tag': $scope.seachDataMode.tag}, function () {
                });
                chrome.storage.sync.set({'activeResumeMode': true}, function () {
                });
                $scope.loadonstartup();
                chrome.runtime.sendMessage({
                    action: 'updateIcon',
                    value: true
                });
                window.close();
            };

            $scope.counts = 0;
            //$scope.extractedData = {};

            /*If Domain not found, please empty the company url */
            $scope.clearComUrl = function () {
                if ($scope.extractedData.companyWebsiteUrl === "Domain not found. Please enter here manually.") {
                    $scope.extractedData.companyWebsiteUrl = "";
                }
            };

            $scope.mouseenter = function () {
                $scope.team = true;
            };
            $scope.mouseover = function () {
                $scope.team = false;
            };

            /* Raise a support ticket */
            $scope.riseSupport = function () {
                // $scope.isSaveProfile = false;
                $scope.extnLoader = true;
                aloreFactory.riseSupportForFailure(frmLocal)
                    .then(function (res) {
                        $scope.isSaveProfile = false;
                        // $scope.succMsg2 = true;
                        $scope.extnSuccessMsg4 = "Support ticket successfully generated. We will contact you shortly.";
                        $scope.extnLoader = false;
                    }, function (res) {
                        // $scope.succMsg2 = true;
                        $scope.extnSuccessMsg4 = "We are facing some issue. Please try again later.";
                    });
            };

            //get companyWebsiteUrl and assign to extractedData
            var companyWebsiteUrlInterval = null;
            var oldExtCompanyWebsiteUrl = '';
            var newExtCompanyWebsiteUrl = '';

            /* When you are unable to find domain automatically */
            var endFun = setTimeout(function () {
                companyWebsiteUrlInterval = setInterval(function () {
                    chrome.storage.sync.get('extCompanyWebsiteUrl', function (r) {
                        newExtCompanyWebsiteUrl = r['extCompanyWebsiteUrl'];
                        if (newExtCompanyWebsiteUrl === 'Domain not found. Please enter here manually.' || newExtCompanyWebsiteUrl === '' || newExtCompanyWebsiteUrl === null || newExtCompanyWebsiteUrl == undefined || newExtCompanyWebsiteUrl == "undefined") {
                            newExtCompanyWebsiteUrl = 'Domain not found. Please enter here manually.';
                            $scope.showSupport = true;
                            $scope.extnLoader = false;
                         /*   $scope.extnSuccessMsg0 = "Domain name could not be found because of the following reasons: ";
                            $scope.extnSuccessMsg1 = "1. Company's profile page couldn't be loaded due to a slow internet connection.";*/
                           /* $scope.retrymsg = "Try Again.";*/
                          /*  $scope.extnSuccessMsg2 = "2. Domain name of the company is not mentioned in the company page. You can still   obtain the email address by manually filling in the domain   name.";*/
                            $scope.ormsg = "or";
                            $timeout(function () {
                                $scope.extnSuccessMsg0 = "";
                                $scope.extnSuccessMsg1 = "";
                                $scope.extnSuccessMsg2 = "";
                                $scope.ormsg = "";
                                $scope.retrymsg = "";
                            }, 5000);
                            clearTimeout(endFun);
                        }

                        var tcount = $timeout(function (count) {
                            $scope.extractedData.companyWebsiteUrl = newExtCompanyWebsiteUrl;
                            if (oldExtCompanyWebsiteUrl !== newExtCompanyWebsiteUrl) {
                                oldExtCompanyWebsiteUrl = newExtCompanyWebsiteUrl;
                                $scope.loader = false;
                                clearInterval(companyWebsiteUrlInterval);
                            }
                        }, 0);
                    });
                }, 500);
            }, 7000);

            /* Save Search Data to Enter it as a prospect */
            $scope.saveData = function () {
                $scope.extnLoader = true;
                aloreFactory.profileExtractorDataSave($scope.extractedData, frmLocal)
                    .then(function (res) {
                        $scope.loader = false;
                        $scope.showSingleSearch = true;
                        $scope.extnLoader = false;
                        // $scope.onLoaderVisible = true;
                        $scope.extnSuccessMsg = "User data saved successfully.";
                        $scope.extractedData = {};
                        $scope.viewProfileMode = true;
                    }, function (res) {
                        $scope.showSupport = true;
                        if (res.status === 400) {
                            $scope.extnSuccessMsg = "Contact already exists";
                        }
                        if (res.status === 402) {
                            $scope.extnSuccessMsg = "Email Not Found";
                        }
                        if (res.status === 404) {
                            $scope.extnSuccessMsg = "Problem in connecting to server. Please check your internet connection";
                        }
                        if (res.status === 420) {
                            $scope.extnSuccessMsg = "Domain Name Not Found.  Do you want to enter manually.";
                        }
                        if (res.status === 421) {
                            $scope.extnSuccessMsg = "Email credits not available. Please Buy or Refer to get more credits.";
                        }
                        if (res.status === 422) {
                            $scope.extnSuccessMsg = "Oops!Something went wrong. Please Contact CRM Support.";
                        }
                        if (res.status === 500) {
                            $scope.extnSuccessMsg = "Oops!Something went wrong. Please try again.";
                        }
                        $scope.extnLoader = false;
                        // $scope.onLoaderVisible = true;
                        // $timeout(function () {
                        //     $scope.extnSuccessMsg = "";
                        // }, 5000);
                    });
            };

            /* Logout from extension */
            $scope.logOutExtn = function () {
                localStorage.removeItem("usrCompObj");
                $state.go("login");
            };

            /* View Prospect Profile On CRM */
            $scope.viewUserProile = function () {
                $window.open("https://crm.alore.io/#/dashboard/prospect");
            };

            /* Disable everything while working */
            $scope.extractedData = {};
            $scope.disableButton = function () {
                setTimeout(function () {
                    if (!$scope.extractedData){
                        $scope.extractedData = {};
                    }
                    if ($scope.viewProfileMode || $scope.extractedData.name == 'undefined' || !$scope.extractedData.name || !$scope.extractedData.companyWebsiteUrl || !$scope.extractedData.profileurl) {
                        return true;
                    }
                    if ($scope.extractedData.name || $scope.extractedData.companyWebsiteUrl || $scope.extractedData.profileurl) {
                        $scope.viewProfileMode = false;
                    }
                },2000);
            };

            /* Get all tags according to which you want to search */
            $scope.tagList = [];
            $scope.getAllTags = function () {
                aloreFactory.getAllTags(frmLocal)
                    .then(function (res) {
                        $scope.tagList = res.data.list;
                    }, function (res) {

                    });
            };
            $scope.getAllTags();

            /* Extract Host Name */
            function extractHostname(url) {
                var hostname;
                //find & remove protocol (http, ftp, etc.) and get hostname
                if (url.indexOf("://") > -1) {
                    hostname = url.split('/')[2];
                }
                else {
                    hostname = url.split('/')[0];
                }
                //find & remove port number
                hostname = hostname.split(':')[0];
                //find & remove "?"
                hostname = hostname.split('?')[0];

                return hostname;
            }

            function getParameterByName(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }

            /* Open Setting Tab */
            $scope.viewSettings = function () {
                $scope.closeAllTabs()
                $scope.linkedinSettingsPage = true;
            };

            /* Open Search Tab */
            $scope.viewSearchTab = function () {
                if($scope.settingInfo){
                    $scope.closeAllTabs()
                    $scope.searchMediaPage = true;
                    if($scope.multiSearch){
                        $scope.showSearchExtractSettings = true;
                    }else{
                        $scope.showSingleSearch = true;
                    }
                }
            };

            /* LinkedIn Setting lists */
            $scope.linkedAccTypeList = ["Free Account", "Premium Account"];
            $scope.hourlyCountList = [{'count': 10, 'plan': 'Slow: under 10 per hour'}, {
                'count': 30,
                'plan': 'Medium: around 30 per hour'
            }, {'count': 50, 'plan': 'Fast: over 50 per hour'}];
            $scope.dailyCountList = [{'count': 200, 'plan': 'Slow: under 200 per day'}, {
                'count': 400,
                'plan': 'Medium: around 400 per day'
            }, {'count': 500, 'plan': 'Fast: over 500 per day'}];

            /* Save Extension Settings */
            $scope.saveEstensionSettings = function () {
                aloreFactory.saveExtensionSettings(frmLocal, $scope.settingObj)
                    .then(function (res) {
                        $scope.extnSuccessMsg4 = "Settings successfully saved";
                        $scope.settingInfo = true;
                        $scope.searchMediaPage = true;
                        $scope.linkedinSettingsPage = false;
                    }, function (res) {
                        $scope.settingInfo = false;
                        $scope.extnSuccessMsg4 = "Something went wrong. Please try again.";
                        $scope.searchMediaPage = false;
                        $scope.linkedinSettingsPage = true;
                    });
                $timeout(function () {
                    $scope.extnSuccessMsg4 = "";
                }, 5000);
            };
        }])

    // login controller
    .controller("loginCtrl", ["$scope", "$location", "aloreFactory", "$state", "$rootScope", "$timeout", function ($scope, $location, aloreFactory, $state, $rootScope, $timeout) {
        $scope.version = version;
        $scope.successClick = function () {
            var loginmode = 2;
            aloreFactory.crmLogin($scope.compDetails, loginmode)
                .success(function (res) {
                    var companyId   = res.companyid;
                    var userId      = res.userid;

                    localStorage.setItem("usrCompObj", JSON.stringify({
                        companyId: companyId,
                        userId: userId,
                        token: res.token
                    }));
                    // set data object to maintain current working and count for email finder
                    // workingMode: 0 = stop, 1 = started, 2 = paused
                    localStorage.setItem("aloreObj", JSON.stringify({workingMode: 0}));
                    $state.go("prospectList");
                }).error(function (res, status) {
                if (status === 401) {
                    $scope.errMsg1 = "Invalid Password";
                }
                if (status === 402) {
                    $scope.errMsg1 = "Invalid email. Please try again with valid email.";
                }
                if (status === 403) {
                    $scope.errMsg1 = "User Email id not verified";
                }
                if (status === 404) {
                    $scope.errMsg1 = "Problem in connecting to server. Please check your internet connection";
                }
                if (status === 500) {
                    $scope.errMsg1 = "Oops!Something went wrong. Please try again";
                }
                $timeout(function () {
                    $scope.errMsg1 = "";
                }, 5000);
            });
        };

        $scope.openSignUp = function () {
            window.open("https://accounts.alore.io/#/signup?returnto=crm.alore.io", "_blank");
        };

        $scope.openForgotPassword = function () {
            window.open("https://accounts.alore.io/#/resetpassword", "_blank");
        };

    }]);